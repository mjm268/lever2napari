# lever2napari

open source tool for converting between LEVER and Napari data formats.

![2D demo](other/2D.gif)

## Getting started

Create a virtual python environment using:

`python3 -m venv <path to where you want your venv stored>`

To activate the environment, use:

`source <venv path/bin/activate>`

Now you can install python packages exclusive to this venv.

## Python dependencies

Reference the `requirements.txt` file for dependencies.

## Usage

`napari_2D.py` and `napari_3D.py` can be used to translate and port your CTC LEVER data into Napari.
2D datasets must be accessible using the LEVER API in order to grab the necessary images.
To use your data, modify this line in either script:

`dataset = "Fluo-N2DH-SIM+_challenge_01.LEVER" # change this for your data`

This project is maintained under the MIT License by Michael J. Martin.