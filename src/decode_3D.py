from pathlib import Path
import numpy as np
import pandas as pd
import sqlite3
import struct
import napari
import json

# load sample data
dataset = "Fluo-C3DH-A549-SIM_training_01.LEVER" # 3D
lever_path = Path("data/3d/"+dataset)
con = sqlite3.connect(lever_path)
tblCells = pd.read_sql_query("select * from tblCells", con)
cursor = con.cursor()
cursor.execute("SELECT jsConstants FROM tblConstants")
constants = json.loads(cursor.fetchall()[0][0])
scale = constants["imageData"]["PixelPhysicalSize"]
dims = constants["imageData"]["Dimensions"]

# convert and reshape verts
vert = tblCells["verts"][0]
vert = np.array(list(vert), dtype=np.int8)
V = vert.view(np.float32)
V = V.reshape(-1,3)
print(V.shape)

# convert and reshape faces
face = tblCells["faces"][0]
face = np.array(list(face), dtype=np.int8)
F = face.view(np.uint32)
F = F.reshape(-1,3)
print(F.shape)

# convert and reshape edges
edge = tblCells["edges"][0]
edge = np.array(list(edge), dtype=np.int8)
E = edge.view(np.uint32)
E = E.reshape(-1,2)
print(E.shape)

# convert and reshape normals
normal = tblCells["normals"][0]
normal = np.array(list(normal), dtype=np.int8)
N = normal.view(np.float32)
N = N.reshape(-1,3)
print(N.shape)

# convert and reshape u16pixels (points)
points = tblCells["u16pixels"][0]
points = np.array(list(points), dtype=np.int8)
P = points.view(np.uint16)
P = P.reshape(-1,3)
print(P.shape)

# napari viewer test
viewer = napari.Viewer(ndisplay=3)
surface = viewer.add_surface(data=(V, F))
surface.scale = scale

# enable normals and wireframe
surface.normals.face.visible = False
surface.normals.vertex.visible = False
surface.wireframe.visible = True

if __name__ == '__main__':
    napari.run()