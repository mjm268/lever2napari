import cv2
import json
import napari
import sqlite3
from pyparsing import col
import requests
import numpy
import numpy as np
import pandas as pd
from pathlib import Path

"""
Purpose: demonstrate capability of 2D LEVER data transformation and compatibility
with Napari. Must have local CTC data available as well as access to API.

Any additional analysis is not automatically saved. This functionality has yet
to be added.
"""

''' connect to restful api '''
api = "https://leverjs.net/ctc2021/g*ctc2021_final*2d*"
dataset = "Fluo-N2DH-SIM+_challenge_01.LEVER" # short lineage
method = "/constants"

try:
    response = requests.get(api+dataset+method)
except:
    print("Could not access API - connection needed to obtain images.")
    exit()

''' get jpegs from api '''
num_frames = response.json()["imageData"]["NumberOfFrames"]
im_dim = response.json()["imageData"]["Dimensions"][0:2] # just want the x and y dimensions
im_dim = [im_dim[1],im_dim[0]] # gotta flip!

im_channel = []
for i in range(1,num_frames+1):
    frame_response = requests.get(api+dataset+"/image/"+str(i))
    frame = cv2.imdecode(np.frombuffer(frame_response.content, np.uint8), -1)
    im_channel.append(frame)
im_channel = np.array(im_channel) # our JPEG images

''' get cell information from db (api is slow) '''
lever_path = Path("data/2d/"+dataset)
con = sqlite3.connect(lever_path)
tblCells = pd.read_sql_query("select * from tblCells", con)

time_groups = tblCells.groupby("time")
cells_by_frame = [time_groups.get_group(x) for x in time_groups.groups]

''' lineage properties '''
families = requests.get(api+dataset+"/lineage")
rowsLineage = pd.DataFrame(families.json()["rowsLineage"]) 
rowsLineage.trackID_parent.fillna(rowsLineage.trackID,inplace=True)
lineage_props = rowsLineage.copy() 
lineage_props.rename(columns={"t0":"t","rootTrackID":"root","trackID_parent":"parent"},inplace=True)
lineage_props.drop(columns=["t1","trackID","jsColor"],inplace=True)

''' lineage graph for mitotic events'''
graph = np.array(rowsLineage[["trackID","trackID_parent"]].values.tolist())
g = {}
for i in graph:
    if i[0] != i[1]:
        g[str(int(i[0]))] = int(i[1])
g = {int(k): v for k, v in g.items()}

''' data to transform and populate '''
centroids_by_frame = []
surfaces_by_frame = []
tracks_by_frame = []

for frame_df in cells_by_frame: # cells at each time-frame
    ''' centroids '''
    centroids = np.array(frame_df[["time","centroid"]].values.tolist())
    centroids_resized = np.empty((centroids.shape[0],3),dtype=np.float16)
    centroids_resized[:,0] = [int(i)-1 for i in centroids[:,0]] # time (frame)
    centroids_resized[:,1] = [eval(i)[1] for i in centroids[:,1]] # x coord (flipped)
    centroids_resized[:,2] = [eval(i)[0] for i in centroids[:,1]] # y coord (flipped)
    [centroids_by_frame.append(i) for i in centroids_resized]

    ''' surfaces '''
    frame_canvas = numpy.zeros(shape=im_dim,dtype=int)
    surfaces = np.array(frame_df[["trackID","surface"]].values.tolist())
    for trackID_row in surfaces:
        trackID = int(trackID_row[0])
        for point in np.flip(np.array(eval(trackID_row[1])),1): # draw boundaries on "canvas"
            frame_canvas[point[0]-1,point[1]-1] = trackID
    surfaces_by_frame.append(frame_canvas)

    ''' tracks '''
    tracks = np.array(frame_df[["trackID"]].values.tolist())
    lineage = np.hstack((tracks,centroids_resized)) # concatenate arrays
    [tracks_by_frame.append(i) for i in lineage]


''' add layers to viewer '''
viewer = napari.Viewer(ndisplay=2) # 2D viewer
image_layer = viewer.add_image(im_channel,name="Channel 1")
surface_layer = viewer.add_labels(np.array(surfaces_by_frame), name="surfaces")
track_layer = viewer.add_tracks(tracks_by_frame, name="tracks",tail_length=0,tail_width=0,properties=lineage_props,graph=g)

''' launch viewer '''
try:
    viewer.window.add_plugin_dock_widget(
        plugin_name="napari-arboretum", widget_name="Arboretum"
    )
except:
    print("Arboretum installation not found - skipping...")

if __name__ == "__main__":
    napari.run()

''' exporting layers to LEVER '''
''' current support is only for labels and tracks of 2D data'''
layers = viewer.layers
for layer in layers:
    if layer._type_string == "Labels":
        pass # TODO
    elif layer._type_string == "Tracks":
        pass # TODO
    else:
        print("layer type not yet supported: ")
print("done - work saved to LEVER database file: ")