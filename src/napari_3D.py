import cv2
import json
import napari
import sqlite3
from pyparsing import col
import requests
import numpy
import numpy as np
import pandas as pd
from pathlib import Path

"""
Purpose: demonstrate capability of 3D LEVER data transformation and compatibility
with Napari. Must have local CTC data available.

Any additional analysis is not automatically saved. This functionality has yet
to be added.
"""

''' connect to local db '''
dataset = "Fluo-C3DH-A549-SIM_training_01.LEVER" # single 3D cell
dataset = "Fluo-N3DH-CHO_training_01.LEVER" # 3D lineage
lever_path = Path("data/3d/"+dataset)
con = sqlite3.connect(lever_path)
tblCells = pd.read_sql_query("select * from tblCells", con)
cursor = con.cursor()
cursor.execute("SELECT jsConstants FROM tblConstants")
constants = json.loads(cursor.fetchall()[0][0])

''' datset info '''
scale = constants["imageData"]["PixelPhysicalSize"]
im_dim = constants["imageData"]["Dimensions"]
im_dim = [im_dim[1],im_dim[0],im_dim[2]] # gotta flip x & y!
num_frames = constants["imageData"]["NumberOfFrames"]

time_groups = tblCells.groupby("time")
cells_by_frame = [time_groups.get_group(x) for x in time_groups.groups]

''' get lineage properties from API '''
api = "https://leverjs.net/ctc2021/g*ctc2021_final*3d*"
families = requests.get(api+dataset+"/lineage")
rowsLineage = pd.DataFrame(families.json()["rowsLineage"]) 
rowsLineage.trackID_parent.fillna(rowsLineage.trackID,inplace=True)
lineage_props = rowsLineage.copy() 
lineage_props.rename(columns={"t0":"t","rootTrackID":"root","trackID_parent":"parent"},inplace=True)
lineage_props.drop(columns=["t1","trackID","jsColor"],inplace=True)

''' lineage graph for mitotic events'''
graph = np.array(rowsLineage[["trackID","trackID_parent"]].values.tolist())
g = {}
for i in graph:
    if i[0] != i[1]:
        g[str(int(i[0]))] = int(i[1])
g = {int(k): v for k, v in g.items()}

''' data to transform and populate '''
centroids_by_frame = []
verts_by_frame = []
faces_by_frame = []
tracks_by_frame = []
pts_by_frame = []

for t,frame_df in enumerate(cells_by_frame): # cells at each time-frame
    ''' centroids '''
    centroids = np.array(frame_df[["time","centroid"]].values.tolist())
    centroids_resized = np.empty((centroids.shape[0],4),dtype=np.float16)
    centroids_resized[:,0] = [int(i)-1 for i in centroids[:,0]] # time (frame)
    centroids_resized[:,1] = [eval(i)[1] for i in centroids[:,1]] # x coord (flipped)
    centroids_resized[:,2] = [eval(i)[0] for i in centroids[:,1]] # y coord (flipped)
    centroids_resized[:,3] = [eval(i)[2] for i in centroids[:,1]] # z coord
    [centroids_by_frame.append(i) for i in centroids_resized]

    ''' surfaces (vertices and faces)'''
    # all cells using points
    V = []
    vt = []
    verts = np.array(frame_df["verts"])
    for vert in verts:
        vert = np.array(list(vert), dtype=np.int8)
        V = vert.view(np.float32)
        V = V.reshape(-1,3)
        V[:,[0,1]] = V[:,[1,0]] # swap x and y!
        [vt.append(np.insert(v,0,t)) for v in V]
    # verts_by_frame.append(vt)
    [pts_by_frame.append(i) for i in vt]

    # F = []
    # faces = frame_df["faces"]
    # [F.append(np.array(list(f), dtype=np.int8)) for f in faces]
    # for f in F:
    #     for index,i in enumerate(f.view(np.uint32).reshape(-1,3)):
    #         faces_by_frame.append(i) 

    ''' tracks '''
    tracks = np.array(frame_df[["trackID"]].values.tolist())
    lineage = np.hstack((tracks,centroids_resized)) # concatenate arrays
    [tracks_by_frame.append(i) for i in lineage]


''' add layers to viewer '''
viewer = napari.Viewer(ndisplay=3) # 3D viewer
pts = viewer.add_points(pts_by_frame,size=3,opacity=0.3)
pts.scale = scale
track_layer = viewer.add_tracks(tracks_by_frame, name="tracks",tail_length=0,tail_width=0,properties=lineage_props,graph=g,scale=scale)

''' surface configs '''
# surface_layer = viewer.add_surface(data=(np.array(verts_by_frame), np.array(faces_by_frame)), name="surfaces")
# surface_layer.scale = scale
# surface_layer.normals.face.visible = False
# surface_layer.normals.vertex.visible = False
# surface_layer.wireframe.visible = True

''' launch viewer '''
try:
    viewer.window.add_plugin_dock_widget(
        plugin_name="napari-arboretum", widget_name="Arboretum"
    )
except:
    print("Arboretum installation not found - skipping...")

if __name__ == "__main__":
    napari.run()

''' exporting layers to LEVER '''
''' current support is only for labels and tracks of 2D data'''
layers = viewer.layers
for layer in layers:
    if layer._type_string == "Labels":
        pass # TODO
    elif layer._type_string == "Tracks":
        pass # TODO
    else:
        print("layer type not yet supported: ")
# print("done - work saved to LEVER database file: ")